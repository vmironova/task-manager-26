package ru.t1consulting.vmironova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email);

    void login(
            @Nullable String login,
            @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}
