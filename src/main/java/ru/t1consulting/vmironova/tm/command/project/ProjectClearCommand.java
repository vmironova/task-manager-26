package ru.t1consulting.vmironova.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final String userId = getUserId();
        getProjectService().clear(userId);
    }

}
